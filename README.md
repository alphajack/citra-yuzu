# Citra and Yuzu

Binaries of Citra and Yuzu from Match 4th, 2024

Sources:
- https://github.com/PabloMK7/citra/ (citra updated version)
- https://archive.org/download/citra-nightly-2104 (citra march 4th version)
- https://archive.org/download/citra-latest-builds-4th-march-2024 (citra march 4th version)
- https://archive.org/download/yuzu-latest-builds-4th-march-2024 (yuzu march 4th version, unavailable)
